import pytest


@pytest.mark.asyncio
async def test_get_kv_v1(hub, ctx, vault_kv_v1_secret):
    ret = await hub.exec.vault.secrets.kv_v1.secret.get(
        ctx, path=vault_kv_v1_secret["path"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    secret = ret["ret"]
    assert vault_kv_v1_secret["data"] == secret.get("data")
    assert vault_kv_v1_secret["path"] == secret.get("path")
